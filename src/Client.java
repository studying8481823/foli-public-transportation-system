// class that simulates a client and properties that is important for our system
public class Client {
    //@classInvariant
    //balance, restOfValidTime, cardId cannot be null or empty
    //balance cannot be negative
    //restOfValidTime must be positive
    private double balance;
    private long restOfValidTime;
    private int cardId;

    private Client(double balance, int cardId) {
        this.balance = balance;
        this.restOfValidTime = 0;
        this.cardId = cardId;
    }

    //@.pre
    //@.post RESULT != null && RESULT == cardId
    public int getCardId() {
        return cardId;
    }

    //@.pre
    //@.post RESULT != null && RESULT == balance
    public double getBalance() {
        return balance;
    }

    //@.pre
    //@.post RESULT != null && RESULT == restOfValidTime
    public long getRestOfValidTime() {
        return restOfValidTime;
    }

    //@.pre balance >= 0
    //@.post this.balance = balance
    // not used to change balance by setter
    private void setBalance(double balance) {
        this.balance = balance;
    }

    //@.pre restOfValidTime >= 0
    //@.post this.restOfValidTime = restOfValidTime
    // not used to change restOfValidTime by setter
    private void setRestOfValidTime(long restOfValidTime) {
        this.restOfValidTime = restOfValidTime;
    }

    //@.pre amount > 0
    //@.post (balance > OLD (balance))
    // not used currently, but can be used later
    public void addBalance(double amount) {
        this.balance += amount;
    }

    //@.pre amount > 0
    //@.post (balance < amount && balance == OLD (balance)) || (balance >= amount && balance < OLD (balance))
    // important method that allows us to interact with balance in purchase routine
    public boolean deductBalance(double amount) {

        if(this.balance>=amount) {
            this.balance -= amount;
            return true;
        }
        return false;
    }

    //@.pre validDuration > 0
    //@.post (currentTime < validDuration && restOfValidTime == OLD (restOfValidTime)) || (currentTime >= validDuration && restOfValidTime > OLD (restOfValidTime))
    // method that updates chosen Client's restOfValidTime
    public void updateValidTime(long validDuration) {
        long currentTime = System.currentTimeMillis();

        if(currentTime < validDuration) {
            restOfValidTime = Math.max(restOfValidTime, currentTime + validDuration);
        } else {
            restOfValidTime = currentTime + validDuration;
        }
    }
}
