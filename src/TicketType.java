// this enum class represents tickets that client can purchase
public enum TicketType {
    SINGLE(3, 7200000), //1000 * 60 * 60 * 2
    DAY(8, 86400000), //1000 * 60 * 60 * 24
    MONTH(55, 2592000000L); //1000 * 60 * 60 * 24 * 30

    private final double price;
    private final long duration;


    TicketType (double price, long duration) {
        this.price = price;
        this.duration = duration;
    }

    //@.pre
    //@.post RESULT != null && RESULT == price
    public double getPrice() {
        return price;
    }

    //@.pre
    //@.post RESULT != null && RESULT == duration
    public long getDuration() {
        return duration;
    }
}
