// this class simulates physical card that client uses in transport
public class PhysicalCard {
    //@classInvariant
    //cardId cannot be null or empty
    //cardId must be positive integer
    int cardId;

    //@.pre publicationYear > 0
    //@.post this.cardId == cardId
    public PhysicalCard (int cardId){
        this.cardId = cardId;
    }

    //@.pre
    //@.post RESULT != null && RESULT == cardId
    public int getCardId() {
        return cardId;
    }
}
