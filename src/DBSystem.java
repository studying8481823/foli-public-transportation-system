import java.util.HashMap;

// this class simulates our DB
// wanted to have this system separate of any chosen transport for sake of safety and it seemed logic to me
// after all any important data shouldn't be located in transport or, of course, on clients card
public class DBSystem implements TicketPurchaseSystem {
    //@classInvariant currentValidClients != null
    //@classInvariant !currentValidClients.containsValue(null)
    private HashMap<Integer, Client> currentValidClients; // we use HashMap for it's ability to quickly find a client by cardId

    public DBSystem () {
        currentValidClients = new HashMap<>();
    }

    //@.pre cardId > 0
    //@.post RESULT == Client || RESULT == null
    public Client getClient(int cardId) {
        return currentValidClients.get(cardId);
    }

    //@.pre client != null
    //@.post OLD currentValidClients.size() + 1
    public void addClient(Client client) {
        currentValidClients.put(client.getCardId(), client);
    }

    //@.pre currentValidClients.get(cardId) != null
    //@.post OLD currentValidClients.size() - 1
    public void removeClient(int cardId) {
        currentValidClients.remove(cardId);
    }


    //@.pre currentValidClients.containsKey(cardId)
    //@.pre validDuration == ticketType.getDuration()
    //@.post ???
    // method that changes the amount of restOfValidTime of DB's Client-object
    public void updateValidTime (int cardId, long validDuration) {
        Client client = currentValidClients.get(cardId);

        if (client !=null) {
            long currentTime = System.currentTimeMillis();
            client.updateValidTime(validDuration + currentTime);
        }
    }

    //@.pre client != null
    //@.post client.deductBalance(price) == true -> client.getRestOfValidTime() > OLD(client.getRestOfValidTime())
    //@.post client.deductBalance(price) == false -> client.getRestOfValidTime() == OLD(client.getRestOfValidTime())
    // method that checks is it possible to buy a ticket that Client chose
    @Override
    public boolean purchaseTicket(Client client, TicketType ticketType) {
        double price = ticketType.getPrice();
        long validDuration = ticketType.getDuration();

        if (client.deductBalance(price)) {
            updateValidTime(client.getCardId(), validDuration);
            return true;
        }
        return false;
    }
}
