import java.util.HashMap;

// class represents the physical reading device in the transport through which client can realise a trip
public class ReadingDevice {
    //@classInvariant currTrip != null
    //@classInvariant !currTrip.containsValue(null)
    private HashMap<Integer, Client> currTrip; //used to check if the client payed for the current trip
    private TicketPurchaseSystem ticketPurchaseSystem;
    private DBSystem dbSystem;

    public ReadingDevice (TicketPurchaseSystem ticketPurchaseSystem, DBSystem dbSystem) {
        this.ticketPurchaseSystem = ticketPurchaseSystem;
        this.dbSystem = dbSystem;
        currTrip = new HashMap<>();
    }

    //@.pre card != null
    //@.post (dbSystem.getClient(card.getCardId()) != null OUTPUT == "Your balance is " + client.getBalance()) || (dbSystem.getClient(card.getCardId()) == null OUTPUT == "Card not found.")
    // method which gives an option to check the balance using reading device
    public void balanceCheck (PhysicalCard card) {
        int cardId = card.getCardId();
        Client client = dbSystem.getClient(cardId);

        if (client != null) {
            System.out.println("Your balance is " + client.getBalance());
        } else {
            System.out.println("Card not found.");
        }
    }

    //@.pre card != null
    //@.post if client != null -> || (OUTPUT == "Thank you for using our transport!" && currTrip.put(cardId, client)
    //@.post if client == null -> OUTPUT == "Card not found."
    //@.post currTrip.size() <= OLD(currTrip.size())
    // this method starts interaction with client
    public void cardRead (PhysicalCard card) {
        int cardId = card.getCardId();
        Client client = dbSystem.getClient(cardId);

        if(client != null) {
            if(System.currentTimeMillis() > client.getRestOfValidTime()) {
                ticketChoice(client);
            } else {
                System.out.println("Thank you for using our transport!");
                currTrip.put(cardId, client);
            }
        } else {
            System.out.println("Card not found.");
        }

        // didn't know where is better implement this routine
        // seemed to me kind of logic to use the pause after interaction with client
        removeExpiredClients();
    }

    //@.pre !currTrip.isEmpty()
    //@.post currTrip.size() <= OLD(currTrip.size())
    // the method which removes Clients from the local "DB"
    private void removeExpiredClients() {
        long currentTime = System.currentTimeMillis();
        currTrip.keySet().removeIf(cardId -> dbSystem.getClient(cardId).getRestOfValidTime() < currentTime);
    }

    //@.pre client != null
    //@.post (client.getRestOfValidTime() > OLD(client.getRestOfValidTime()) && OUTPUT == "Ticket purchased successfully!") || (client.getRestOfValidTime() == OLD(client.getRestOfValidTime() && OUTPUT == "Insufficient funds on the card.")
    // the method which realises routine for buying a "ticket"
    private void ticketChoice(Client client) {
        System.out.println("Choose a ticket: ");
        System.out.println("1. Single ticket (2 hours) - 3 euro");
        System.out.println("2. Day ticket (24 hours) - 8 euro");
        System.out.println("3. Month ticket (30 days) - 55 euro");

        // choice is made outside of this system, through reading device's interface
        int choice  = 1;
        TicketType ticketType = TicketType.SINGLE;

        switch (choice) {
            case 1:
                ticketType = TicketType.SINGLE;
                break;
            case 2:
                ticketType = TicketType.DAY;
                break;
            case 3:
                ticketType = TicketType.MONTH;
                break;
            default:
                System.out.println("Please, choose a ticket.");
                return;
        }

        if (ticketPurchaseSystem.purchaseTicket(client, ticketType)) {
            System.out.println("Ticket purchased successfully!");
        } else {
            System.out.println("Insufficient funds on the card.");
        }
    }
 }
