public interface TicketPurchaseSystem {
    //@.pre client != null
    //@.post
    boolean purchaseTicket(Client client, TicketType ticketType);
}
