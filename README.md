Turku regional public transportation Föli wants to modernize its payment card system to be Java-based. The operation of the cards is simplified so that the card can be loaded with X euros. The current balance of the card can also be inquired. Amounts may also include individual euro cents. Devices suitable for these operations will be placed in buses and public transport service points.

In addition, when boarding a bus, one can pay for the journey using the card on the bus's reader device. When purchasing a journey, the device offers options to buy

a single ticket (valid for 2 hours, price 3 euros),
a day ticket (valid for 24 hours, price 8 euros),
or a monthly ticket (valid for 30 days, price 55 euros).
The price of the ticket is deducted from the card. If a previously purchased ticket is still valid, there is no need to buy a new ticket for a transfer. A transfer ticket also does not extend the validity of the previously purchased ticket. Of course, if one boards the bus during the ticket's validity period, one may travel until the bus route's service ends. This does not need to be recorded in the systems. If the card does not have enough loaded money, a ticket cannot be purchased, and traveling on the bus is not possible. To enable purchasing, more money must be loaded onto the card.

In the implementation, it has been decided to use the method System.currentTimeMillis(), which returns the system time in milliseconds, to track time. For example,

the duration of a 2-hour ticket is 100060602 milliseconds,

a 24-hour ticket 1000606024,

and a 30-day ticket 100060602430 milliseconds.

Time can thus be conveniently compared to the expiration time calculated when buying a ticket. It is assumed that all bus clocks are synchronized outside this system using the NTP protocol and any inaccuracy in clock synchronization is to the detriment of the customer.

 

Task: Model the public transportation payment card system with the described precision using object design principles.

Define classes, methods, and member variables, and all related agreements (routine definitions, class invariants, and encapsulation).

Provide brief justifications for your choices in the verbal descriptions. Pay special attention to whose responsibility each aspect of the plan is, which data types you end up with, and how the system's parts communicate with each other. The description's result is an abstract functional model of the system and is not expected to work together with physical payment terminals, cards, and other systems at this stage.

You can also implement the program if you wish, but just a specification is enough. The implementation could, for example, print descriptions of different operations' functioning on the screen.